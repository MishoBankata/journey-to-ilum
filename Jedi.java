public class Jedi {
    private int posi;
    private int posj;
    private int hp = 100;
    private int attack;
    Jedi(int posi, int posj) {
    	setPosi(posi, 0);
    	setPosj(posj, 0);
    }
    
    //getters
    public int getPosi() {
        return posi;
    }
    public int getPosj() {
        return posj;
    }
    public int getHp() {
		return hp;
	}
    
    //setters
    public void setPosi(int posi, int move) {
    	this.posi = posi + move;
    }
    public void setPosj(int posj, int move) {
		this.posj = posj + move;
    }
    public void setHp(int hp, int change) {
		this.hp = hp - change;
	}
    public void setAttack(int attack) {
		this.attack = attack;
	}
    
	
  //wip
	public void attack() {
		hp = hp - attack;
	}
	public void lightning() {
		hp = hp - 25;
	}
	public void heal() {
		hp = hp + 15;
	}
	
}
