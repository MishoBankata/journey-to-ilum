import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class GameFrame extends Frame {
	Map map;
	private Label label;
	Label info = label;
	Label health = label;
	private Button button;
	Button fight = button;
	Button feed = button;
	Button pet = button;
	Button moveUp = button;
	Button moveDown = button;
	Button moveLeft = button;
	Button moveRight = button;
	Button start1 = button;
	Button start2 = button;
	
	private TextArea textArea;
	TextArea introduction = textArea;
	
	GameFrame(Map k) {
		
		
		
		map = k;
		GameWindowListener gwl = new GameWindowListener();
		
		addWindowListener(gwl);
		
		this.setBackground(Color.BLACK);
		
		this.setLayout(null);
		
		start2 = new Button("Click here to start the game");
		start2.setForeground(Color.YELLOW);
		start2.setBackground(Color.BLACK);
		start2.setFont(new Font("Tahoma", Font.BOLD, 14));
		start2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				start2.setVisible(false);
				introduction.setVisible(false);
			}
		});
		start2.setBounds(1000, 250, 350, 50);
		this.add(start2);
		start2.setVisible(false);
		
		start1 = new Button("Click here to hide the introduction");
		start1.setForeground(Color.YELLOW);
		start1.setBackground(Color.BLACK);
		start1.setFont(new Font("Tahoma", Font.BOLD, 14));
		start1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				start1.setVisible(false);
				start2.setVisible(true);
				introduction.setText("\n\n\n\n\n"
						+ "The game is played only with the mouse"
						+ "\n\n"
						+ "Don't click on the movement buttons too quickly to avoid bugs and glitches."
						+ "\n\n"
						+ "Good luck and have fun :)");
			}
		});
		start1.setBounds(1000, 250, 350, 50);
		this.add(start1);
		
		
		
		introduction = new TextArea();
		introduction.setText("\n"
				+ "Star Wars: Journey to Ilum\n\n"
				+ "You are a Jedi Padawan in the final stages of their training.\n"
				+ "To complete your training and become a true Jedi,\n"
				+ "you will need to craft a lightsaber.\n"
				+ "You already have your hilt, but the lightsaber\n"
				+ "needs a power source - a kyber crystal.\n"
				+ "\n"
				+ "Like every other Jedi, you must travel to the frozen planet Ilum\n"
				+ "and search for a crystal in the planet's caves.\n"
				+ "Ilum is rich in kyber crystals, but not just any crystal will do.\n"
				+ "You must use the Force to find the one right for you.\n"
				+ "The Jedi Council's orders were clear - return with a kyber crystal,\n"
				+ "or do not return at all.\n\n"
				+ "May the Force be with you...");
		introduction.setVisible(true);
		introduction.setForeground(Color.YELLOW);
		introduction.setBackground(Color.BLACK);
		introduction.setFont(new Font("Tahoma", Font.BOLD, 34));
		introduction.setBounds(0, 30, 2000, 1000);
		this.add(introduction);
		
		
		info = new Label("You stand at the entrance of the cave");
		info.setVisible(true);
		info.setForeground(Color.YELLOW);
		info.setBackground(Color.BLACK);
		info.setFont(new Font("Tahoma", Font.BOLD, 20));
		info.setBounds(50, 630, 750, 88);
		this.add(info);
		
		health = new Label("HP: " + map.jedi.getHp());
		health.setVisible(true);
		health.setForeground(Color.YELLOW);
		health.setBackground(Color.BLACK);
		health.setFont(new Font("Tahoma", Font.BOLD, 40));
		health.setBounds(470, 480, 180, 60);
		this.add(health);
		
		fight = new Button("Fight the creature");
		fight.setForeground(Color.YELLOW);
		fight.setBackground(Color.BLACK);
		fight.setFont(new Font("Tahoma", Font.BOLD, 14));
		fight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 2 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 9) {
					info.setText("You punch the beast and it cuts you with its sharp claws.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 50);
				} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 10 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 23) {
					info.setText("You take a swing at the the Razhak, but break your fingers on its scales.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 30);
				} else {
					info.setText("You kick the tiny Lisk and it runs away.");
					map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] = 70;
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fight.setVisible(false);
					feed.setVisible(false);
					pet.setVisible(false);
					moveUp.setVisible(true);
					moveDown.setVisible(true);
					moveLeft.setVisible(true);
					moveRight.setVisible(true);
				}
				health.setText("HP:" + map.jedi.getHp());
				repaint();
			}
		});
		fight.setBounds(50, 440, 350, 50);
		this.add(fight);
		
		feed = new Button("Feed the creature");
		feed.setForeground(Color.YELLOW);
		feed.setBackground(Color.BLACK);
		feed.setFont(new Font("Tahoma", Font.BOLD, 14));
		feed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 2 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 9) {
					info.setText("You give the beast some food, but it bites your hand off.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 70);
				} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 10 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 23) {
					info.setText("You throw the Razhak some food. Satisfied, it burrows back in the cave wall.");
					map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] = 50;
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fight.setVisible(false);
					feed.setVisible(false);
					pet.setVisible(false);
					moveUp.setVisible(true);
					moveDown.setVisible(true);
					moveLeft.setVisible(true);
					moveRight.setVisible(true);
				} else {
					info.setText("The lisk takes the food you gave it and throws it at your face.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 5);
				}
				health.setText("HP:" + map.jedi.getHp());
				repaint();
			}
		});
		feed.setBounds(50, 500, 350, 50);
		this.add(feed);
		
		pet = new Button("Pet the creature");
		pet.setForeground(Color.YELLOW);
		pet.setBackground(Color.BLACK);
		pet.setFont(new Font("Tahoma", Font.BOLD, 14));
		pet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 2 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 9) {
					info.setText("You scratch the beast behind the ears and it falls asleep.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fight.setVisible(false);
					feed.setVisible(false);
					pet.setVisible(false);
					moveUp.setVisible(true);
					moveDown.setVisible(true);
					moveLeft.setVisible(true);
					moveRight.setVisible(true);
					map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] = 90;
				} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 10 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 23) {
					info.setText("You try to pet the Razhak, but it wraps around your arm and squeezes it hard.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 20);
				} else {
					info.setText("The lisk doesn't like to be touched and bites your hand.");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					map.jedi.setHp(map.jedi.getHp(), 10);
				}
				health.setText("HP:" + map.jedi.getHp());
				repaint();
			}
		});
		pet.setBounds(50, 560, 350, 50);
		this.add(pet);
		
		fight.setVisible(false);
		feed.setVisible(false);
		pet.setVisible(false);
		
		
		
		
		moveUp = new Button("Move North");
		moveUp.setForeground(Color.YELLOW);
		moveUp.setBackground(Color.BLACK);
		moveUp.setFont(new Font("Tahoma", Font.BOLD, 14));
		moveUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.jedi.getPosi() > 0) {
					map.jedi.setPosi(map.jedi.getPosi(), -1);
				}
				repaint();
			}
		});
		moveUp.setBounds(175, 440, 100, 50);
		this.add(moveUp);
		
		moveDown = new Button("Move South");
		moveDown.setForeground(Color.YELLOW);
		moveDown.setBackground(Color.BLACK);
		moveDown.setFont(new Font("Tahoma", Font.BOLD, 14));
		moveDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.jedi.getPosi() < 6) {
					map.jedi.setPosi(map.jedi.getPosi(), 1);
					repaint();
				} else repaint();
			}
		});
		moveDown.setBounds(175, 540, 100, 50);
		this.add(moveDown);
		
		moveLeft = new Button("Move West");
		moveLeft.setForeground(Color.YELLOW);
		moveLeft.setBackground(Color.BLACK);
		moveLeft.setFont(new Font("Tahoma", Font.BOLD, 14));
		moveLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.jedi.getPosj() > 0) {
					map.jedi.setPosj(map.jedi.getPosj(), -1);
					repaint();
				} else repaint();
			}
		});
		moveLeft.setBounds(125, 490, 100, 50);
		this.add(moveLeft);
		
		moveRight = new Button("Move East");
		moveRight.setForeground(Color.YELLOW);
		moveRight.setBackground(Color.BLACK);
		moveRight.setFont(new Font("Tahoma", Font.BOLD, 14));
		moveRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (map.jedi.getPosj() < 6) {
					map.jedi.setPosj(map.jedi.getPosj(), 1);
					repaint();
				} else repaint();
			}
		});
		moveRight.setBounds(225, 490, 100, 50);
		this.add(moveRight);
		
	}
	
	public void paint(Graphics g) {
		
		
		
		BufferedImage hero = null;
		BufferedImage happyhero = null;
		BufferedImage sadhero = null;
		BufferedImage deadhero = null;
		BufferedImage centipede = null;
		BufferedImage entrance = null;
		BufferedImage cliffs = null;
		BufferedImage stalagmites = null;
		BufferedImage crystal = null;
		BufferedImage lisk = null;
		BufferedImage panther = null;
		BufferedImage gorgodon = null;
		BufferedImage death = null;
		BufferedImage kyber = null;
		
		File you = new File("frown.png");
		File bigyou = new File("bigHeroHappy.png");
		File sadyou = new File("bigHeroSad.png");
		File deadyou = new File("bigHeroDead.png");
		File centi = new File("centipede.png");
		File ent = new File("caveEntrance.png");
		File cl = new File("cliffs.png");
		File stal = new File("stalagmites.png");
		File cr = new File("crystal.png");
		File lis = new File("lisk.png");
		File pan = new File("panther.png");
		File gor = new File("gorgodon.png");
		File dth = new File("dead.png");
		File kbr = new File("kyber.png");
		
		try {
			hero = ImageIO.read(you);
			happyhero = ImageIO.read(bigyou);
			sadhero = ImageIO.read(sadyou);
			deadhero = ImageIO.read(deadyou);
			centipede = ImageIO.read(centi);
			entrance = ImageIO.read(ent);
			cliffs = ImageIO.read(cl);
			stalagmites = ImageIO.read(stal);
			crystal = ImageIO.read(cr);
			panther = ImageIO.read(pan);
			lisk = ImageIO.read(lis);
			gorgodon = ImageIO.read(gor);
			death = ImageIO.read(dth);
			kyber = ImageIO.read(kbr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	    g.setColor(Color.YELLOW);
	    g.drawImage(hero, 50 + map.jedi.getPosj() * 50 , 50 + map.jedi.getPosi() * 50, null);
		for (int i = 0; i < map.getDisj(); i++) {
			for (int j = 0; j < map.getDisi(); j++) {
				g.drawRect(50 + i * 50 , 50 + j * 50,  50,  50);
			}
		}
		
		g.drawImage(sadhero, 450, 100, null);
		
		if (map.jedi.getPosi() == map.getLoci() && map.jedi.getPosj() == map.getLocj()) {
			try {
				File b = new File("smile.png");
				try {
					hero = ImageIO.read(b);
				} catch (IOException e) {
					e.printStackTrace();
				}
				info.setText("You have found your kyber crystal!");
				g.drawImage(kyber, 850, 150, null);
				Thread.sleep(4000);
				
				g.drawImage(hero, 50 + map.jedi.getPosj() * 50 , 50 + map.jedi.getPosi() * 50, null);
				g.drawImage(happyhero, 450, 100, null);
				
				g.drawImage(hero, 50 + map.jedi.getPosj() * 50 , 50 + map.jedi.getPosi() * 50, null);
				
				for (int i = 0; i < map.getDisj(); i++) {
					for (int j = 0; j < map.getDisi(); j++) {
						g.drawRect(50 + i * 50 , 50 + j * 50,  50,  50);
					}
				}
				
				info.setText("You put the crystal in the hilt and it lights up.");
				Thread.sleep(4000);
				info.setText("Your task on Ilum is done and you are now a true Jedi.");
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(0);
		}
		
		
		
		if (map.jedi.getPosi() == 6 && map.jedi.getPosj() == 6) {
			info.setText("You stand at the entrance of a frozen cave.");
			g.drawImage(entrance, 850, 175, null);
			fight.setVisible(false);
			feed.setVisible(false);
			pet.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 1) {
			info.setText("You stumble upon a Gorgodon. The beast stomps you immediately.");
			map.jedi.setHp(map.jedi.getHp(), 100);
			g.drawImage(gorgodon, 850, 35, null);
			fight.setVisible(false);
			feed.setVisible(false);
			pet.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 2 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 9) {
			info.setText("An Asharl Panther leaps in front of you.");
			g.drawImage(panther, 850, 200, null);
			fight.setVisible(true);
			feed.setVisible(true);
			pet.setVisible(true);
			moveUp.setVisible(false);
			moveDown.setVisible(false);
			moveLeft.setVisible(false);
			moveRight.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 10 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 23) {
			info.setText("A Razhak is poking out of the cave walls.");
			g.drawImage(centipede, 900, 175, null);
			fight.setVisible(true);
			feed.setVisible(true);
			pet.setVisible(true);
			moveUp.setVisible(false);
			moveDown.setVisible(false);
			moveLeft.setVisible(false);
			moveRight.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 24 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 39) {
			info.setText("A puny lisk is blocking your path.");
			g.drawImage(lisk, 900, 200, null);
			fight.setVisible(true);
			feed.setVisible(true);
			pet.setVisible(true);
			moveUp.setVisible(false);
			moveDown.setVisible(false);
			moveLeft.setVisible(false);
			moveRight.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 40 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 59) {
			info.setText("You see many kyber crystals here, but none are calling to you.");
			g.drawImage(crystal, 850, 200, null);
			fight.setVisible(false);
			feed.setVisible(false);
			pet.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 60 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 79) {
			info.setText("This part of the cave is full of giant stalactites and stalagmites.");
			g.drawImage(stalagmites, 850, 175, null);
			fight.setVisible(false);
			feed.setVisible(false);
			pet.setVisible(false);
		} else if (map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] >= 80 && map.stuff[map.jedi.getPosi()][map.jedi.getPosj()] <= 99) {
			info.setText("You stand on the edge of a deep ravine.");
			g.drawImage(cliffs, 850, 175, null);
			fight.setVisible(false);
			feed.setVisible(false);
			pet.setVisible(false);
		}
		
		
		if (map.jedi.getHp() <= 0) {
			info.setText("You have died due to injuries from Ilum's monsters, GAME OVER");
//			g.drawImage(death, 50 + map.jedi.getPosj() * 50 , 50 + map.jedi.getPosi() * 50, null);
			g.drawImage(deadhero, 450, 100, null);
			g.drawImage(death, 50 + map.jedi.getPosj() * 50 , 50 + map.jedi.getPosi() * 50, null);
			for (int i = 0; i < map.getDisj(); i++) {
				for (int j = 0; j < map.getDisi(); j++) {
					g.drawRect(50 + i * 50 , 50 + j * 50,  50,  50);
				}
			}
			health.setText("HP: " + 0);
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.exit(0);
		}
		
		
		
//		if (map.jedi.getPosi() == map.lisk.getPosi() && map.jedi.getPosj() == map.lisk.getPosj()) {
//			fight.setVisible(true);
//			feed.setVisible(true);
//			pet.setVisible(true);
//			map.lisk.setPosi(map.lisk.getPosi(), 10);
//		}
	}
}
