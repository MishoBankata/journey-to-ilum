public class Map {
	private int disi;
	private int disj;
	private int loci;
	private int locj;
	
	int[][] stuff = new int[7][7];
	
	Jedi jedi;
	
	Map(int disi, int disj, Jedi j, int[][] st) {
		this.setDisi(disi);
		this.setDisj(disj);
		jedi = j;
		stuff = st;
	}
	
	//getters
	public int getDisi() {
		return disi;
	}
	public int getDisj() {
		return disj;
	}
	public int getLoci() {
		return loci;
	}
	public int getLocj() {
		return locj;
	}
	
	//setters
	public void setDisi(int disi) {
		this.disi = disi;
	}
	public void setDisj(int disj) {
		this.disj = disj;
	}
	public void setLoci(int loci) {
		this.loci = loci;
	}
	public void setLocj(int locj) {
		this.locj = locj;
	}
	
}
