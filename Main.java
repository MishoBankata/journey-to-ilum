import java.util.Random;

public class Main {
    public static void main(String args[]) throws InterruptedException {
        Random rand = new Random();
        
        
        Jedi player = new Jedi(6, 6);
        
        
        int[][] stuf = new int[7][7];
        
        for (int i = 0; i < 7; i++) {
        	for (int j = 0; j < 7; j++) {
        		stuf[i][j] = rand.nextInt(100);
        	}
        }
        
//        CREATURES 40%
//        0-1 gorgodon 2%
//        2-9 asharl panther 8%
//        10-23 razhak 14%
//        24-39 lisk 16%
//        
//        TERRAIN 60%
//        40-59 crystal caves 20%
//        60-79 stalagmites 20%
//        80-99 cliffs 20%
        
        
        Map m = new Map(7, 7, player, stuf);
        int loci = rand.nextInt(7);
        int locj = rand.nextInt(7);
        while (true) {
        	if((loci == player.getPosi() && locj == player.getPosj()) || loci == (player.getPosi() - 1) || locj == (player.getPosj() - 1)) {
            	loci = rand.nextInt(7);
            	locj = rand.nextInt(7);
            } else break;
        }
        
        m.setLoci(loci);
        m.setLocj(locj);
        
        GameFrame f = new GameFrame(m);
        f.setSize(f.getMaximumSize());
        f.setVisible(true);
    }
}